(function () {
    'use strict';
    const assign = require('object-assign');
    const format = require('util').format;
    const mongo = require('mongodb');
    const monk = require('monk');
    const defaults = {
        mongodbUrl: process.env.MONGODB_URL,
        databaseName: process.env.MONGODB_DATABASE_NAME || 'DEFAULT',
        recycleInterval: process.env.MONGODB_RECYCLE_INTERVAL || 60*10
    };

    const connectionPool = {};
    const poolId = Math.floor(Math.random()*10000);

    function hashCode(str) {
        return 'Hash' + Math.abs(str.split('').reduce((prevHash, currVal) =>
            (((prevHash << 5) - prevHash) + currVal.charCodeAt(0)) | 0, 0));
    }

    function checkConnection(connection, options) {
        return new Promise(function (resolve, reject) {
                if (!(connection && connection.db)) {
                    reject();
                }
                const connectionCheckAge = (new Date() - connection.lastConnectionCheckAt) / 1000;
                if (options.recycleInterval && connectionCheckAge >= options.recycleInterval) {
                    connection.db.close(true,
                        function (err, result) {
                            reject(err);
                        })
                } else {
                    resolve();
                }
            }
        )
    }

    function middlewareWrapper(o) {
        // if options are static (either via defaults or custom options passed in), wrap in a function
        let optionsCallback = null;
        if (typeof o === 'function') {
            optionsCallback = o;
        } else {
            optionsCallback = function (req, cb) {
                cb(null, o);
            };
        }

        return function tenantMiddleware(req, res, next) {
            optionsCallback(req, function (err, options) {
                if (err) {
                    next(err);
                } else {
                    const o = assign({}, defaults, options);
                    const databaseName = o.databaseName;
                    const hash = hashCode(o.mongodbUrl + databaseName);
                    
                    const promise = checkConnection(connectionPool[hash], o)
                        .then((db) => {
                            if (process.env.DEVELOPMENT) {
                                console.log('reusing connection from pool: ' + hash);
                            }
                            req.db = connectionPool[hash].db;
                            next();
                        })
                        .catch((err) => {
                            const url = format(o.mongodbUrl, databaseName)
                            monk(url).then((db)=>{
                                console.log('Successfuly created new connection to: ' + databaseName);
                                connectionPool[hash] = {db: db, lastConnectionCheckAt: new Date()}
                                db.on('timeout', () => {
                                    process.env.DEVELOPMENT && console.log('Mongo connection lost')
                                    delete connectionPool[hash]
                                })
                                db.on('close', () => {
                                    process.env.DEVELOPMENT && console.log('Mongo connection closed')
                                    delete connectionPool[hash]
                                })

                                req.db = db;
                                next();
                            }).catch((err)=>{
                                console.error('FAILED creating new connection to: ' + databaseName+' ERROR: ' + JSON.stringify(err) );
                                next(err);
                            });

                        })
                }
            })
        };
    }

    module.exports = middlewareWrapper;
}());
